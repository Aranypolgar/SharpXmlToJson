﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    public class WriteToJson
    {
        public static void Write(string str, string filename)
        {
            System.IO.File.WriteAllText(filename + ".json", str);
        }                
    }
}
