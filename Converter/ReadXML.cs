﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Converter
{
    public class ReadXML
    {
        private readonly string Path;
        public XmlDocument xml = new XmlDocument();

        public ReadXML(string path)
        {
            this.Path = path;
        }

        public void GetXmlFile()
        {
            FileStream fs = 
                new FileStream(this.Path, FileMode.Open, FileAccess.Read);
            this.xml.Load(fs);
            fs.Dispose();
        }

        // Test 
        [Conditional("DEBUG")]
        public void  WriteOut()
        {
            this.xml.Save(Console.Out);
        }
        

    }

}
