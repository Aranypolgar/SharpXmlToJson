﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Formatting = Newtonsoft.Json.Formatting;

namespace Converter
{
    public class XmlToJson
    {     
        public static void Convert(XmlDocument xml, out string json)
        {
            json = JsonConvert.SerializeXmlNode(xml, Formatting.Indented, true);  
        }
    }
}
