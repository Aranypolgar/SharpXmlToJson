﻿using Converter;
using System;

namespace ConverterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadXML xML = new ReadXML("cd.xml");

            xML.GetXmlFile();

            string json;
            XmlToJson.Convert(xML.xml, out json);

            WriteToJson.Write(json, "converted");
            
        }
    }
}
